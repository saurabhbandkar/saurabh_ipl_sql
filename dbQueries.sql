sudo -i -u postgres
psql

CREATE DATABASE ipl;
USE ipl;
CREATE TABLE IF NOT EXISTS matches (
    id Int PRIMARY KEY,
    season Int,
    city varchar(255),
    date Date,
    team1 varchar(255),
    team2 varchar(255),
    toss_winner varchar(255),
    toss_decision varchar(255),
    result varchar(255),
    dl_applied bit,
    winner varchar(255),
    win_by_runs Int,
    win_by_wickets Int,
    player_of_match varchar(255),
    venue varchar(255),
    umpire1 varchar(255),
    umpire2 varchar(255),
    umpire3 varchar(255));

CREATE TABLE IF NOT EXISTS deliveries (
    match_id Int,
    inning Int,
    batting_team varchar(255),
    bowling_team varchar(255),
    `over` Int,
    ball Int,
    batsman varchar(255),
    non_striker varchar(255),
    bowler varchar(255),
    is_super_over bit,
    wide_runs Int,
    bye_runs Int,
    legbye_runs Int,
    noball_runs Int,
    penalty_runs Int,
    batsman_runs Int,
    extra_runs Int,
    total_runs Int,
    player_dismissed varchar(255),
    dismissal_kind varchar(255),
    fielder varchar(255),
    FOREIGN KEY (match_id) REFERENCES matches(id));


\COPY matches
FROM '/tmp/matches.csv'
DELIMITER ',' CSV HEADER;

\COPY deliveries
FROM '/tmp/deliveries.csv'
DELIMITER ',' CSV HEADER;


1. Number of matches played per year for all the years in IPL

SELECT season, COUNT(season) FROM matches GROUP BY season;


2. Number of matches won per team per year in IPL.

SELECT winner, season, COUNT(season) FROM matches GROUP BY season,winner ORDER BY winner;


3. Extra runs conceded per team in the year 2016

SELECT bowling_team,SUM(extra_runs) FROM matches 
INNER JOIN deliveries 
ON matches.id = deliveries.match_id 
WHERE season = 2016 
GROUP BY bowling_team;


4. Top 10 economical bowlers in the year 2015

SELECT bowler, ROUND(((SUM(batsman_runs)+SUM(wide_runs)+SUM(noball_runs))*6.0/COUNT(bowler)),2) AS economy FROM matches 
INNER JOIN deliveries 
ON matches.id = deliveries.match_id 
WHERE season = 2015 
GROUP BY bowler 
ORDER BY economy 
LIMIT 10;